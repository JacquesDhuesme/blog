<?php
namespace App;

use PDO;
use Exception;

class PaginatedQuery {

    private $query;
    private $queryCount;
    private $pdo;
    private $perPage;
    private $count;
    private $items;

    public function __construct(
        string $query,
        string $queryCount,
        ? PDO $pdo = null,
        int $perPage = 12
    )
    {
        $this->query = $query;
        $this->queryCount = $queryCount;
        $this->pdo = $pdo ?: Connection::getPDO();
        $this->perPage = $perPage;
    }

    public function getItems(string $classMapping): array{
        if($this->items === null){
            $currentPage = $this->getCurrentpage();
            $pages = $this->getPages();
            if($currentPage > $pages){
                throw new Exception("Cette page n'éxiste pas");
            }
            $offset = $this->perPage * ($currentPage -1);
        $this->items = $this->pdo->query(
                $this->query .
                " LIMIT {$this->perPage} OFFSET $offset")
                ->fetchAll(PDO::FETCH_CLASS, $classMapping);
       }
       return $this->items;
    }

    public function previousLink(string $link): ?string{
        $currentPage = $this->getCurrentpage();
        if ($currentPage <= 1) return null;
        if ($currentPage > 2) $link .= "?page=" . ($currentPage - 1);
        return <<<HTML
            <a href="{$link}" class="btn btn-primary">&laquo; Page Précedente</a>
HTML;
    }

    public function nextLink(string $link): ?string{
        $currentPage = $this->getCurrentpage();
        $pages = $this->getPages();
        if ($currentPage >= $pages) return null;
        $link .= "?page=" . ($currentPage + 1);
        return <<<HTML
            <a href="{$link}" class="btn btn-primary ml-auto">Page Suivante &raquo;</a>
HTML;
    }

    private function getCurrentpage(): int{
        return Url::getPositiveInt('page', 1);
    }

    private function getPages(): int{
        if($this->count === null){
            $this->count = (int)$this->pdo
                ->query($this->queryCount)
                ->fetch(PDO::FETCH_NUM)[0];
        }
        return (int)ceil($this->count / $this->perPage);
    }
}