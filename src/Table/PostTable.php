<?php
namespace App\Table;

use App\PaginatedQuery;
use App\Model\Post;
use Exception;

final class PostTable extends Table{

    protected $table = 'post';
    protected $class = Post::class;
    
    public function findPaginated() {
        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM {$this->table} ORDER BY created_at DESC",
            "SELECT count(id) FROM {$this->table}"
        );
        $posts = $paginatedQuery->getItems(Post::class);
        (new CategoryTable($this->pdo))->hydratePosts($posts);
        return[$posts, $paginatedQuery];
    }

    public function findPaginatedForCategory(int $categoryId){
        $paginatedQuery = new PaginatedQuery(
            "SELECT p.* 
                FROM {$this->table} AS p
                JOIN post_category AS pc ON pc.post_id = p.id
                WHERE pc.category_id = {$categoryId}
                ORDER BY p.created_at DESC",
            "SELECT count(*) 
                FROM post_category 
                WHERE category_id = {$categoryId}"
        );
        
        $posts = $paginatedQuery->getItems(Post::class);
        (new CategoryTable($this->pdo))->hydratePosts($posts);
        return[$posts, $paginatedQuery];
    }

    public function updatePost(Post $post): void{
        $this->update([
            'id' => $post->getId(),
            'name' => $post->getName(),
            'slug' => $post->getSlug(),
            'content' => $post->getContent(),
            'created_at' => $post->getCreatedAt()->format('Y-m-d H:i:s')
        ], $post->getId());
    }

    public function createPost(Post $post): void{
        $id = $this->create([
            'name' => $post->getName(),
            'slug' => $post->getSlug(),
            'content' => $post->getContent(),
            'created_at' => $post->getCreatedAt()->format('Y-m-d H:i:s')
        ]);
        $post->setId($id);
    }

    public function attachCategories(int $id, array $categories): void{
        $this->pdo->exec('DELETE FROM post_category WHERE post_id = ' . $id);
        $query = $this->pdo->prepare('INSERT INTO post_category SET post_id = ?, category_id = ?');
        foreach($categories as $category){
            $query->execute([$id, $category]);
        }
    }
}