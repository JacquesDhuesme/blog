<?php
namespace App\Table;

use PDO;
use App\Model\Category;

final class CategoryTable extends Table{

    protected $table = "category";
    protected $class = Category::class;

    public function hydratePosts (array $posts): void{
        $postById = [];
        foreach($posts as $post){
            $post->setCategories([]);
            $postById[$post->getId()] = $post;
        }
        $categories = $this->pdo
            ->query("SELECT c.*, pc.post_id
            FROM post_category pc
            JOIN category c ON c.id = pc.category_id
            WHERE pc.post_id IN (" . implode(',', array_keys($postById)) . ")"
            )->fetchAll(PDO::FETCH_CLASS, Category::class);

        foreach($categories as $category){
            $postById[$category->getPostId()]->addCategory($category);
        }
    }

    public function all(){
        return $this->queryAndFetchAll("SELECT * FROM {$this->table} ORDER BY id DESC");
    }

    public function list() : array{
        $categories = $this->queryAndFetchAll("SELECT * FROM {$this->table} ORDER BY name ASC");
        $result = [];
        foreach ($categories as $category){
            $result[$category->getId()] = $category->getName();
        }
        return $result;
    }
}