<?php

use App\Connection;
use App\HTML\Form;
use App\Model\Category;
use App\Table\CategoryTable;
use App\Validators\CategoryValidator;
use App\Auth;

Auth::check();

$errors = [];
$item = new Category();
$fields = ['name','slug'];

if (!empty($_POST)){
    $pdo = Connection::getPDO();
    $table = new CategoryTable($pdo);
    $validator = new CategoryValidator($_POST, $table);
    if ($validator->validate()){
        App\ObjectHelper::hydrate($item,$_POST,$fields);
        $id = $table->create([
            'name' => $item->getName(),
            'slug' => $item->getSlug()
        ]);
        $item->setId($id);
        header('Location: ' . $router->url('admin_category_edit', ['id' => $item->getId()]) . '?created=1');
        exit();

    }else{
        $errors = $validator->errors();
    }
}

$form = new Form($item, $errors);
?>

<?php if (!empty($errors)): ?>
<div class="alert alert-danger">
    L'article n'a pas pu être créé, merci de corriger vos erreurs
</div>
<?php endif; ?>

<h1>Créer une catégorie</h1>

<form action="" method="POST">
    <?= $form->input('name', 'Titre'); ?>
    <?= $form->input('slug', 'URL'); ?>
    <button class="btn btn-primary">Enregistrer</button>
</form>