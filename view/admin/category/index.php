<?php

use App\Auth;
use App\Connection;
use App\Table\CategoryTable;
use App\Table\PostTable;

Auth::check();

$title = "Gestion des categories";
$pdo = Connection::getPDO();
$items = (new CategoryTable($pdo))->all();
$link = $router->url('admin_categories');
?>

<h1>Administation articles</h1>
<?php if (isset($_GET['delete'])): ?>
<div class="alert alert-success">
    L'enregistrement à bien été supprimé
</div>
<?php endif; ?>
<table class="table">
    <thead>
        <th>#</th>
        <th>Titre</th>
        <th>URL</th>
        <th><a href="<?= $router->url('admin_category_new') ?>" class="btn btn-primary">Nouveau</a></th>
    </thead>
    <tbody>
        <?php foreach ($items as $item): ?>
        <tr>
            <td><?= $item->getId() ?></td>
            <td><?= htmlentities($item->getName()) ?></td>
            <td><?= htmlentities($item->getSlug()) ?></td>
            <td>
                <a href="<?= $router->url('admin_category_edit', ['id' => $item->getId() ]) ?>" class="btn btn-primary">Modifier</a>
                <form action="<?= $router->url('admin_category_delete', ['id' => $item->getId() ]) ?>" method="POST" 
                    onsubmit="return confirm('Voulez vous vraiment effectuer cette action')" style="display:inline">
                    <button class="btn btn-danger">Supprimer</button>
                </form>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>