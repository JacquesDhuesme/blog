<?php

use App\Connection;
use App\HTML\Form;
use App\Model\Post;
use App\Table\PostTable;
use App\Validators\PostValidator;
use App\Auth;
use App\Table\CategoryTable;

Auth::check();

$pdo = Connection::getPDO();
$errors = [];
$post = new Post();
$categoryTable = new CategoryTable($pdo);
$categories = $categoryTable->list();
$post->setCreatedAt(date('Y-m-d H:i:s'));

if (!empty($_POST)){
    $postTable = new PostTable($pdo);
    $validator = new PostValidator($_POST, $postTable, $post->getId(), $categories);
    App\ObjectHelper::hydrate($post,$_POST,['name','content','slug','created_at']);
    if ($validator->validate()){
        $pdo->beginTransaction();
        $postTable->createPost($post);
        $postTable->attachCategories($post->getId(), $_POST['categories_ids']);
        $pdo->commit();
        header('Location: ' . $router->url('admin_post_edit', ['id' => $post->getId()]) . '?created=1');
        exit();

    }else{
        $errors = $validator->errors();
    }
}

$form = new Form($post, $errors);
?>

<?php if (!empty($errors)): ?>
<div class="alert alert-danger">
    L'article n'a pas pu être créé, merci de corriger vos erreurs
</div>
<?php endif; ?>

<h1>Nouvel article</h1>

<?php require '_form.php'; ?>