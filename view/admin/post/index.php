<?php

use App\Auth;
use App\Connection;
use App\Table\PostTable;

Auth::check();

$title = "Administration";

$pdo = Connection::getPDO();

[$posts, $pagination] = (new PostTable($pdo))->findPaginated();
$link = $router->url('admin_posts');
?>

<h1>Administation articles</h1>
<?php if (isset($_GET['delete'])): ?>
<div class="alert alert-success">
    L'enregistrement à bien été supprimé
</div>
<?php endif; ?>
<table class="table">
    <thead>
        <th>#</th>
        <th>Titre</th>
        <th><a href="<?= $router->url('admin_post_new') ?>" class="btn btn-primary">Nouveau</a></th>
    </thead>
    <tbody>
        <?php foreach ($posts as $post): ?>
        <tr>
            <td><?= $post->getId() ?></td>
            <td><?= htmlentities($post->getName()) ?></td>
            <td>
                <a href="<?= $router->url('admin_post_edit', ['id' => $post->getId() ]) ?>" class="btn btn-primary">Modifier</a>
                <form action="<?= $router->url('admin_post_delete', ['id' => $post->getId() ]) ?>" method="POST" 
                    onsubmit="return confirm('Voulez vous vraiment effectuer cette action')" style="display:inline">
                    <button class="btn btn-danger">Supprimer</button>
                </form>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div class="d-flex justify-content-between my-4">
    <?= $pagination->previousLink($link) ?>
    <?= $pagination->nextLink($link) ?>
</div>